/*********************************************************************

  Name:         pcie.cxx
  Created by:   Konstantin Olchanski

  Contents:     Test program for PCIE registers and data FIFO
                
  $Id$
*********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <errno.h>
#include <math.h>
#include <unistd.h>
#include <sys/mman.h>
#include <signal.h>
//#include <mmintrin.h>

#define D08(ptr) (*(volatile uint8_t*)(ptr))
#define D16(ptr) (*(volatile uint16_t*)(ptr))
#define D32(ptr) (*(volatile uint32_t*)(ptr))
#define D64(ptr) (*(volatile uint64_t*)(ptr))

#include "PciDevice.h"

uint64_t rdtsc()
{
   uint64_t ret;
   __asm__ __volatile__("rdtsc" : "=A" (ret));
   return ret;
}

static inline void prefetch(void *x) 
{ 
  //__asm__ __volatile__("prefetch %0" :: "m" (*(unsigned long *)x));
  __asm__ __volatile__("prefetchnta %0" :: "m" (*(unsigned long *)x));
}

#if 0
[root@ladd11 pcieadc]# echo "base=0x640000000 size=0x01000000 type=write-through" >> /proc/mtrr
[root@ladd11 pcieadc]# echo "base=0x640000000 size=0x00001000 type=uncachable" >> /proc/mtrr
[root@ladd11 pcieadc]# cat /proc/mtrr
reg00: base=0x00000000 (   0MB), size=16384MB: write-back, count=1
reg01: base=0x400000000 (16384MB), size=8192MB: write-back, count=1
reg02: base=0x600000000 (24576MB), size=1024MB: write-back, count=1
reg03: base=0xc0000000 (3072MB), size=1024MB: uncachable, count=1
reg04: base=0xbf800000 (3064MB), size=   8MB: uncachable, count=1
reg05: base=0x640000000 (25600MB), size=  16MB: write-through, count=1
reg06: base=0x640000000 (25600MB), size=   4KB: uncachable, count=1
[root@ladd11 pcieadc]# echo "disable=5" >> /proc/mtrr
[root@ladd11 pcieadc]# echo "disable=6" >> /proc/mtrr
#endif

/********************************************************************/
#ifdef MAIN_ENABLE
int main(int argc,char* argv[])
{

  setbuf(stdout, NULL);
  setbuf(stderr, NULL);

  signal(SIGILL,  SIG_DFL);
  signal(SIGBUS,  SIG_DFL);
  signal(SIGSEGV, SIG_DFL);
  signal(SIGPIPE, SIG_DFL);

  PciDevice *p = new PciDevice();
  int status = p->open("/dev/altera_pcie");
  if (status != 0) {
    printf("Cannot open PCIE device, status %d\n", status);
    return 1;
  }

  for (int i=1; i<argc; i++) {
    if (0) {
    } else if (strcmp(argv[i],"--sleep")==0) {
      int isleep = strtoul(argv[i+1], NULL, 0);
      printf("sleep %d sec\n", isleep);
      sleep(isleep);
    } else if (strcmp(argv[i],"--read32")==0) {
      int addr = strtoul(argv[i+1], NULL, 0);
      uint32_t r = p->read32(addr);
      printf("pcie addr %d D32 reads 0x%08x\n", addr, r);
    } else if (strcmp(argv[i],"--read64")==0) {
      int addr = strtoul(argv[i+1], NULL, 0);
      uint64_t r = p->read64(addr);
      printf("pcie addr %d D64 reads 0x%016lx\n", addr, r);
    } else if (strcmp(argv[i],"--write32")==0) {
      int addr = strtoul(argv[i+1], NULL, 0);
      uint32_t ival = strtoul(argv[i+2], NULL, 0);
      i++;
      p->write32(addr, ival);
      printf("pcie addr %d wrote 0x%08x\n", addr, ival);
    } else if (strcmp(argv[i],"--write64")==0) {
      int addr = strtoul(argv[i+1], NULL, 0);
      uint64_t ival0 = strtoul(argv[i+2], NULL, 0);
      uint64_t ival1 = strtoul(argv[i+3], NULL, 0);
      i+=2;
      uint64_t ival = ival0 | (ival1 << 32);
      p->write64(addr, ival);
      printf("pcie addr %d wrote 0x%016lx\n", addr, ival);
    } else if (strcmp(argv[i],"--bread64")==0) {
      int iaddr = strtoul(argv[i+1], NULL, 0);
      int inw64 = strtoul(argv[i+2], NULL, 0);
      i++;
      i++;
      uint64_t buf[inw64];
      uint64_t t0 = rdtsc();
      for (int i=0; i<inw64; i++) {
        buf[i] = p->read64(iaddr + i*8);
      }
      uint64_t t1 = rdtsc();
      printf("elapsed %ld\n", t1-t0);
      for (int i=0; i<inw64; i++) {
        printf("pcie addr 0x%04x[%d] reads 0x%016lx\n", iaddr, i, buf[i]);
      }
    } else if (strcmp(argv[i],"--test1")==0) {
      //__mm_prefetch(regs+0*8, 0);
      prefetch(p->base+0*8);
      prefetch(p->base+1*8);
      prefetch(p->base+2*8);
      prefetch(p->base+3*8);
      //__builtin_prefetch(p->base+0*8);
      //__builtin_prefetch(p->base+1*8);
      //__builtin_prefetch(p->base+2*8);
      //__builtin_prefetch(p->base+3*8);
      uint64_t v1 = D64(p->base+0*8);
      uint64_t v2 = D64(p->base+1*8);
      uint64_t v3 = D64(p->base+2*8);
      uint64_t v4 = D64(p->base+3*8);
      printf("test1: 0x%016lx 0x%016lx 0x%016lx 0x%016lx\n", v1, v2, v3, v4);

    } else if (strcmp(argv[i],"--exit")==0) {
      exit(0);
    }
  }

#if 0
  if (0)
     {
        uint64_t v = 0;
        __builtin_prefetch(p->base+0);
        __builtin_prefetch(p->base+4);
        __builtin_prefetch(p->base+8);
        __builtin_prefetch(p->base+16);
        __builtin_prefetch(p->base+32);
        __builtin_prefetch(p->base+64);
        __builtin_prefetch(p->base+2*64);
        __builtin_prefetch(p->base+3*64);
        v += D64(p->base+0);
        v += D64(p->base+8);
        v += D64(p->base+16);
        v += D64(p->base+32);
        v += D64(p->base+64);
        v += D64(p->base+2*64);
        v += D64(p->base+3*64);
        printf("D64 = 0x%016llx\n", v);
        exit(1);
     }

  if (0)
     {
        int i = strtoul(argv[1], NULL, 0);

        uint32_t s = D32(p->base+i);
        printf("D32[0x%04x] = 0x%08x\n",i,s);
	exit(0);
     }

  if (0)
     {
        int i = strtoul(argv[1], NULL, 0);

        uint64_t s = D64(p->base+i);
        printf("D64[0x%04x] = 0x%016llx\n",i,s);
	exit(0);
     }

  if (0)
     {
        int i = strtoul(argv[1], NULL, 0);
        uint64_t v0 = strtoul(argv[2], NULL, 0);
        uint64_t v1 = strtoul(argv[3], NULL, 0);
        uint64_t v = (v1<<32) | v0;
        D64(p->base+i) = v;
        printf("D64[0x%08x] write 0x%016llx\n",i,v);
	exit(0);
     }

  if (0 && argc == 2)
     {
        int i = strtoul(argv[1], NULL, 0);

        __builtin_prefetch(p->base+i);
        uint64_t s = D64(p->base+i);
        printf("D64[0x%08x] = 0x%016llx\n",i,s);
        exit(0);
     }

  if (0 && argc == 3)
     {
        int i = strtoul(argv[1], NULL, 0);
        uint64_t v0 = strtoul(argv[2], NULL, 0);
        uint64_t v1 = 0; // strtoul(argv[3], NULL, 0);
        uint64_t v = (v1<<32) | v0;
        D64(p->base+i) = v;
        printf("D64[0x%08x] write 0x%016llx\n",i, v);
	exit(0);
     }

  if (0)
     {
        uint64_t x = 0;
        for (int i=0; i<100; i++)
           x += D64(p->base + 8*i);
        printf("D64[0x%08x] = 0x%016llx\n",0,x);
        exit(0);
     }

  if (0)
     {
        uint64_t x = 0;
        for (int i=0; i<0x40; i++) {
           uint64_t ii = i;
           uint64_t v = 0;
           v |= 0xAA000000 | i;
           v |= ((0xBB000000|ii)<<32);
           D64(p->base + 8*i) = v;
        }
     }

  if (0) // read FIFO word count
     {
        while (1)
           {
              int i = 0;
              int addr = 0x100 + 8*i;
              uint64_t v = D64(p->base + addr);
              printf("D64[0x%08x] = 0x%016llx - FIFO count\n", addr, v);
              sleep(1);
           }

        exit(1);
     }

  if (0) // read FIFO word count
     {
        int i = 0;
        int addr = 0x100 + 8*i;
        uint64_t v = D64(p->base + addr);
        printf("D64[0x%08x] = 0x%016llx\n", addr, v);
     }

  if (0)
     {
        for (int i=0; i<0x40; i++) {
           int addr = 0x2000 + 8*i;
           uint64_t v = D64(p->base + addr);
           printf("D64[0x%08x] = 0x%016llx\n", addr, v);
        }
     }

  if (0)
     {
        for (int i=0; i<0x80; i++) {
           uint32_t v = D32(p->base + 4*i);
           printf("D32[0x%08x] = 0x%08lx\n", i, v);
        }
     }

  if (0)
     {
        char buf[256];
        memcpy(buf, p->base, 256);
     }

  if (0)
     {
        //[root@daqtmp3 ~]# echo "disable=1" >> /proc/mtrr
        //[root@daqtmp3 ~]# echo "base=0xD0000000 size=0x01000000 type=write-through" >> /proc/mtrr
        //[root@daqtmp3 ~]# cat /proc/mtrr
        //reg00: base=0x00000000 (   0MB), size=2048MB: write-back, count=1
        //reg01: base=0x90000000 (2304MB), size=  16MB: write-through, count=1

        uint64_t x = 0;
        uint64_t t0 = rdtsc();
        __builtin_prefetch(p->base+0);
        __builtin_prefetch(p->base+64);
        for (int i=0; i<100; i++)
           x += D64(p->base + 8*i);
        uint64_t t1 = rdtsc();
        printf("elapsed %lld\n", t1-t0);
        printf("D64[0x%08x] = 0x%16llx\n",0,x);
     }

  if (0)
     {
        int i = strtoul(argv[1], NULL, 0);
        uint32_t v = strtoul(argv[2], NULL, 0);

        D32(p->base+i) = v;
        printf("write D32[0x%08x] = 0x%08x\n",i,v);
     }

  if (0)
     {
        int i = strtoul(argv[1], NULL, 0);
        uint32_t v = strtoul(argv[2], NULL, 0);

        D64(p->base+i) = v;
        printf("write D64[0x%08x] = 0x%08x\n",i,v);
     }

  if (0)
     {
        int i = D32(p->base+0) + D32(p->base+4) + D32(p->base+8) + D32(p->base+12);
     }

  if (0)
     {
        D32(p->base+0) = 0;
        D32(p->base+4) = 1;
        D32(p->base+8) = 2;
        D32(p->base+12) = 3;
     }
#endif

#if 0
  while (1)
    {
      softdac_status(p->base);
      printf("chan DAC%02d\n", 1+chan);
      sleep(1);
    }
#endif

  return 0;
}

#endif
/* end file */

/* emacs
 * Local Variables:
 * mode:C
 * tab-width: 8
 * c-basic-offset: 2
 * End:
 */
