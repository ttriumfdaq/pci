#
# Makefile for the pmc-softdac-m driver
#

all: pcie.exe

clean::
	-rm -fv *.o *.exe

ifdef ROOTSYS
ROOTCFLAGS := -DHAVE_ROOT -I$(ROOTSYS)/include
ROOTLIBS := $(shell $(ROOTSYS)/bin/root-config --glibs)
endif

pcie.o: PciDevice.h

%: %.cxx
	g++ -DMAIN_ENABLE -o $@ -O2 -g -Wall -Wuninitialized $(ROOTCFLAGS) $(ROOTLIBS) $<

%.o: %.cxx
	g++ -c -DMAIN_ENABLE -o $@ -O2 -g -Wall -Wuninitialized $(ROOTCFLAGS) $(ROOTLIBS) $<

%.exe: %.o
	g++ -DMAIN_ENABLE -o $@ -O2 -g -Wall -Wuninitialized $(ROOTCFLAGS) $(ROOTLIBS) $<

#endif
