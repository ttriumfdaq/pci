// PciDevice.h

#include <stdio.h>
#include <stdint.h>

//#define D32(ptr) (*(volatile uint32*)(ptr))
//#define D64(ptr) (*(volatile uint64*)(ptr))

struct PciDevice
{
  char* base;

  ~PciDevice() { base = 0; };

  PciDevice(char*b = 0) { base = b; };

  int open(const char* filename)
  {
    FILE *fp = fopen(filename,"r+");
    if (fp==0) {
      fprintf(stderr,"Cannot open \'%s\', errno: %d (%s)\n", filename, errno,strerror(errno));
      return -errno;
    }

    int fd = fileno(fp);

    base = (char*)mmap(0,0x100000,PROT_READ|PROT_WRITE,MAP_SHARED,fd,0);
    if (base == NULL) {
      fprintf(stderr, "Cannot mmap() \'%s\' registers, errno: %d (%s)\n", filename, errno, strerror(errno));
      return -errno;
    }

    return 0;
  }

  uint32_t read32(int iaddr) { return (*(volatile uint32_t*)(base + iaddr)); }
  uint64_t read64(int iaddr) { return (*(volatile uint64_t*)(base + iaddr)); }
  void write32(int iaddr, uint32_t value) { (*(volatile uint32_t*)(base + iaddr)) = value; }
  void write64(int iaddr, uint64_t value) { (*(volatile uint64_t*)(base + iaddr)) = value; }
};

// end
